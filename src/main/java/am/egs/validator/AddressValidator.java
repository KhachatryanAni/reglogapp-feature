package am.egs.validator;

import am.egs.dto.AddressDto;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("addressValidator")
public class AddressValidator implements Validator<AddressDto> {
    @Override
    public boolean isValid(AddressDto addressDto) {
        if (addressDto.getCity() != null && addressDto.getStreet() != null && addressDto.getApt() > 0)
            return true;
        return false;
    }
}
