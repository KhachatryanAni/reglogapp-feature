package am.egs.validator;

import org.springframework.beans.factory.annotation.Qualifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Qualifier("passwordValidator")
public class PasswordValidator implements Validator<String> {

    /**
     * Validate password with regular expression
     *
     * @param password password for validation
     * @return true valid password, false invalid password
     */

    @Override
    public boolean isValid(String password) {
        String regex = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
//        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(password);
//        boolean matches = matcher.matches();
        return password!=null;
    }
}
