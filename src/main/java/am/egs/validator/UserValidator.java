package am.egs.validator;

import am.egs.dto.AddressDto;
import am.egs.dto.UserDto;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Qualifier("userValidator")
public class UserValidator implements Validator<UserDto> {

    @Override
    public boolean isValid(UserDto user) {
        if (user != null){
            String firstName = user.getFirstName();
            String lastName = user.getLastName();
            String email = user.getEmail();

            String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            boolean matches = matcher.matches();

            if (firstName != null && lastName != null && email != null && matches){
                if (!firstName.equals("") && !lastName.equals("") && !email.equals(""))
                    return true;
            }
        }
        return false;
    }


    static class AddressValidator implements Validator<AddressDto> {
        @Override
        public boolean isValid(AddressDto address) {
            if (address.getCity() != null && address.getStreet() != null && address.getApt() > 0)
                return true;
            return false;
        }
    }

}
