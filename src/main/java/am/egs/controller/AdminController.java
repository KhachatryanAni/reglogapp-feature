package am.egs.controller;

import am.egs.dto.UserDto;
import am.egs.exception.UserNotFoundException;
import am.egs.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.sql.SQLException;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final Logger LOGGER = Logger.getLogger(AdminController.class);

    private UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/welcome")
    public String adminPage(Model model, @SessionAttribute("user") UserDto userDto) {
        model.addAttribute("user", userDto);
        return "welcome";
    }

}
