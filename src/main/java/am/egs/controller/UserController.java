package am.egs.controller;

import am.egs.dto.UserDto;
import am.egs.exception.EmptyDataException;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserAlreadyExistsException;
import am.egs.exception.UserNotFoundException;
import am.egs.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

@RequestMapping("/user")
@Controller
public class UserController extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    private final UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, Model model) throws UserNotFoundException, SQLException {
        model.addAttribute("user", userService.get(id));
        return "welcome";
    }

    @GetMapping("/users")
    public String getAll(Model model) throws SQLException, EmptyDataException {
        model.addAttribute("users", userService.getAll());
        return "employees";
    }

    /*@PostMapping("/update")
    public String update(Model model, @Valid @ModelAttribute UserDto userDto, BindingResult bindingResult) throws UserNotFoundException, InvalidUserException, SQLException {
        if (bindingResult.hasErrors())
            return "errorPage";
        userService.update(userDto);
        return "employees";

    }*/

    @PutMapping("/add")
    public String save(@PathVariable String password, @Valid @ModelAttribute UserDto userDto, BindingResult bindingResult) throws NoSuchAlgorithmException, InvalidUserException, UserAlreadyExistsException, InvalidKeySpecException, SQLException {
        if (bindingResult.hasErrors())
            return "";
        userService.save(userDto, password);
        return "login";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam int id, Model model) throws UserNotFoundException, SQLException, EmptyDataException {
        userService.delete(id);
        model.addAttribute("users", userService.getAll());
        return "adminPage";
    }

}
