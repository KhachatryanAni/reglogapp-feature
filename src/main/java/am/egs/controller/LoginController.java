package am.egs.controller;

import am.egs.dto.LoginDto;
import am.egs.dto.UserDto;
import am.egs.exception.EmptyDataException;
import am.egs.exception.UserNotFoundException;
import am.egs.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

@Controller
@SessionAttributes("user")
public class LoginController {

    private final UserService userService;

    @Autowired
    public LoginController(final UserService userService) {
        this.userService = userService;
    }

    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @GetMapping("/")
    public String entryPoint(LoginDto loginDto) {
        return "login";
    }

    @PostMapping("/login")
    public String login(@Valid LoginDto loginDto, BindingResult bindingResult, Model model) throws NoSuchAlgorithmException, SQLException, InvalidKeySpecException, UserNotFoundException, EmptyDataException {
        UserDto userDto;
        if (bindingResult.hasErrors()) {
            return "login";
        } else {
            userDto = userService.login(loginDto.getEmail(), loginDto.getPassword());
            model.addAttribute("user", userDto);
        }
        if (userDto != null && userDto.isAdmin()) {
            model.addAttribute("user", userDto);
            model.addAttribute("users", userService.getAll());
            return "adminPage";
        }
        return "welcome";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session, Model model){
        session.invalidate();
        model.addAttribute("loginDto", new LoginDto());
        return "login";
    }
}
