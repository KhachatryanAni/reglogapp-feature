package am.egs.controller;

import am.egs.dto.AddressDto;
import am.egs.dto.LoginDto;
import am.egs.dto.RegistrationDto;
import am.egs.dto.UserDto;
import am.egs.exception.EmptyDataException;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserAlreadyExistsException;
import am.egs.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServlet;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

@Controller
public class RegistrationController extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(RegistrationController.class);

    private UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registrationDto", new RegistrationDto());
        return "registration";
    }

    @PostMapping("/register")
    public String register(@RequestParam String password,
                           @Valid @ModelAttribute RegistrationDto registrationDto,
                           BindingResult bindingResult,
                           Model model) throws NoSuchAlgorithmException, InvalidUserException, UserAlreadyExistsException, InvalidKeySpecException, SQLException, EmptyDataException {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        boolean isAdmin = false;
        if (registrationDto.getAdmin() != null && registrationDto.getAdmin().equalsIgnoreCase("fire"))
            isAdmin = true;
        AddressDto addressDto = AddressDto.newBuilder()
                .setCity(registrationDto.getCity())
                .setStreet(registrationDto.getStreet())
                .setApt(registrationDto.getApt()).build();
        UserDto userDto = UserDto.newBuilder()
                .setFirstName(registrationDto.getFirstName())
                .setLastName(registrationDto.getLastName())
                .setEmail(registrationDto.getEmail())
                .setEducation(registrationDto.getEducation())
                .setTelephone(registrationDto.getTelephone())
                .setAdmin(isAdmin)
                .setAddress(addressDto).build();
        userService.save(userDto, password);
        model.addAttribute("user", userDto);
        if (isAdmin) {
            model.addAttribute("users", userService.getAll());
            return "adminPage";
        }
        model.addAttribute("loginDto", new LoginDto(registrationDto.getEmail(), password));
        return "login";
    }

}
