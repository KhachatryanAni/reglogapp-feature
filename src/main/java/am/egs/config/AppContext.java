package am.egs.config;

import am.egs.dto.AddressDto;
import am.egs.dto.UserDto;
import am.egs.dto.mapper.AddressMapper;
import am.egs.dto.mapper.AddressMapperImpl;
import am.egs.dto.mapper.UserMapper;
import am.egs.dto.mapper.UserMapperImpl;
import am.egs.repository.AddressDao;
import am.egs.repository.AddressDaoImpl;
import am.egs.repository.UserDao;
import am.egs.repository.UserDaoImpl;
import am.egs.service.UserService;
import am.egs.service.UserServiceImpl;
import am.egs.validator.AddressValidator;
import am.egs.validator.PasswordValidator;
import am.egs.validator.UserValidator;
import am.egs.validator.Validator;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:hibernate.properties")
@EnableTransactionManagement
public class AppContext {

    @Autowired
    private Environment environment;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[]{"am.egs.entity"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.naming-strategy", environment.getRequiredProperty("hibernate.naming-strategy"));
        return properties;
    }

    @Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    @Bean
    public SessionFactory sessionFactoryBean() {
        return sessionFactory().getObject();
    }

    @Bean
    public Hash hash() {
        return new Hash();
    }

    @Bean
    public UserDao userDao() {
        return new UserDaoImpl(sessionFactoryBean());
    }

    @Bean
    public AddressDao addressDao() {
        return new AddressDaoImpl(sessionFactoryBean());
    }

    @Bean
    public Validator<UserDto> userDtoValidator() {
        return new UserValidator();
    }

    @Bean
    public Validator<AddressDto> addressDtoValidator() {
        return new AddressValidator();
    }

    @Bean
    public AddressMapper addressMapper() {
        return new AddressMapperImpl();
    }

    @Bean
    public UserMapper userMapper() {
        return new UserMapperImpl();
    }

    @Bean
    public Validator<String> passwordValidator() {
        return new PasswordValidator();
    }

    @Bean
    public UserService userService() {
        return new UserServiceImpl(userDao(), addressDao(), userDtoValidator(), addressDtoValidator(), passwordValidator(), userMapper(), hash());
    }
}