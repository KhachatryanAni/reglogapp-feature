package am.egs.repository;

import am.egs.entity.Address;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.sql.SQLException;
import java.util.List;

@Repository

public class AddressDaoImpl implements AddressDao {

    private SessionFactory sessionFactory;

    @Autowired
    public AddressDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Address get(int id) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Address.class, id);
    }

    @Override
    public List<Address> getAll() throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from " + Address.class.getName()).list();
    }

    @Override
    public void save(Address address) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        if (existingEntity(address) == null) {
            session.save(address);
        }
    }

    @Override
    public void update(Address address) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        session.update(address);
    }

    @Override
    public void delete(Address address) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        session.delete(address);
    }

    @Override
    public void deleteById(int id) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        Address address = this.get(id);
        session.delete(address);
    }

    @Override
    public Address existingEntity(Address address) throws SQLException {
        List<Address> addresses = getAll();
        for (Address addressEntity : addresses) {
            if (addressEntity.getCity().name().equalsIgnoreCase(address.getCity().name())
                    && addressEntity.getStreet().equalsIgnoreCase(address.getStreet())
                    && (addressEntity.getApt() == address.getApt())) {
                return addressEntity;
            }
        }
        return null;
    }
}
