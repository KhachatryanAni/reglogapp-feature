package am.egs.repository;

import am.egs.dto.LoginDto;
import am.egs.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.sql.SQLException;
import java.util.List;

@Repository

public class UserDaoImpl implements UserDao {

    private SessionFactory sessionFactory;

    @Autowired
    public UserDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User get(int id) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        return session.get(User.class, id);
    }

    @Override
    public List<User> getAll() throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        return (List<User>) session.createQuery("from " + User.class.getName()).list();
    }

    @Override
    public LoginDto getByEmail(String email) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        Query selectUserByEmail = session.getNamedQuery("User.SelectUserByEmail").setParameter("email", email);
        return (LoginDto) selectUserByEmail.uniqueResult();
    }

    @Override
    public void save(User user) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        session.persist(user);
    }

    @Override
    public void update(User user) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public void deleteByEmail(String email) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from " + User.class.getName() + " u where u.email = :email ");
        query.setParameter("email", email);
        query.executeUpdate();
    }

    @Override
    public void deleteById(int id) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from " + User.class.getName() + " u where u.id = :id ");
        query.setParameter("id", id);
        query.executeUpdate();
    }


    @Override
    public boolean exists(User user) throws SQLException {
        User existingUser = get(user.getId());
        return existingUser != null;
    }

    @Override
    public List<User> getAllWithSameAddress(int addressId) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from " + User.class.getName() + "  u where u.address_id = :addressId ");
        query.setParameter("addressId", addressId);
        List<User> list = query.list();
        return list;
    }
}
