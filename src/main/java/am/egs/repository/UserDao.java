package am.egs.repository;

import am.egs.dto.LoginDto;
import am.egs.entity.User;
import org.hibernate.Session;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    User get(int id) throws SQLException;

    List<User> getAll() throws SQLException;

    LoginDto getByEmail(String email) throws SQLException;

    void save(User user) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException;

    void update(User user) throws SQLException;

    void deleteByEmail(String email) throws SQLException;

    void deleteById(int id) throws SQLException;

    boolean exists(User user) throws SQLException;

    List<User> getAllWithSameAddress(int addressId) throws SQLException;

}
