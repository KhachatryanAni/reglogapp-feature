package am.egs.repository;

import am.egs.entity.Address;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;

public interface AddressDao {

    /**
     * Works with database ''
     *
     * @param  initialCapacity the {@code Properties} will be sized to
     *         accommodate this many elements
     * @throws IllegalArgumentException if the initial capacity is less than
     *         zero.
     */
    Address get(int id) throws SQLException;

    List<Address> getAll() throws SQLException;

    void save(Address address) throws SQLException;

    void update(Address address) throws SQLException;

    void delete(Address address) throws SQLException;

    void deleteById(int id) throws SQLException;

    Address existingEntity(Address address) throws SQLException;

}
