package am.egs.service;

import am.egs.dto.UserDto;
import am.egs.exception.EmptyDataException;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserAlreadyExistsException;
import am.egs.exception.UserNotFoundException;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.List;

public interface UserService {

    UserDto get(int id) throws UserNotFoundException, SQLException;

    UserDto getByEmail(String email) throws UserNotFoundException, SQLException;

    List getAll() throws EmptyDataException, SQLException;

    /*
     * @paar
     *
     * */
    void save(UserDto userDto, String password) throws InvalidUserException, UserAlreadyExistsException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException;

    void update(UserDto user) throws InvalidUserException, UserNotFoundException, SQLException;

    void delete(int id) throws UserNotFoundException, SQLException;

    void deleteByEmail(String email) throws UserNotFoundException, SQLException;

    UserDto login(String email, String password) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException, UserNotFoundException;
}
