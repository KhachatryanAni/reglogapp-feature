package am.egs.service;

import am.egs.config.Hash;
import am.egs.dto.AddressDto;
import am.egs.dto.UserDto;
import am.egs.dto.mapper.UserMapper;
import am.egs.entity.Address;
import am.egs.entity.User;
import am.egs.exception.EmptyDataException;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserAlreadyExistsException;
import am.egs.exception.UserNotFoundException;
import am.egs.repository.AddressDao;
import am.egs.repository.UserDao;
import am.egs.validator.Validator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.List;

@Service
@EnableTransactionManagement
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    private UserDao userDao;

    private AddressDao addressDao;

    private Validator<UserDto> userDtoValidator;

    private Validator<AddressDto> addressDtoValidator;

    private Validator<String> passwordValidator;

    private UserMapper userMapper;

    private Hash hash;

    @Autowired
    public UserServiceImpl(UserDao userDao, AddressDao addressDao, Validator<UserDto> userDtoValidator, Validator<AddressDto> addressDtoValidator, Validator<String> passwordValidator, UserMapper userMapper, Hash hash) {
        this.userDao = userDao;
        this.addressDao = addressDao;
        this.userDtoValidator = userDtoValidator;
        this.addressDtoValidator = addressDtoValidator;
        this.passwordValidator = passwordValidator;
        this.userMapper = userMapper;
        this.hash = hash;
    }

    @Transactional
    @Override
    public UserDto get(int id) throws UserNotFoundException, SQLException {
        User userEntity = userDao.get(id);

        if (userEntity != null) {
            return userMapper.toUserDto(userEntity);
        }
        throw new UserNotFoundException();
    }

    @Transactional
    @Override
    public UserDto getByEmail(String email) throws UserNotFoundException, SQLException {

        /*User userByEmail = userDao.getByEmail(email);

        if (userByEmail != null) {
            return userMapper.toUserDto(userByEmail);
        }*/
        throw new UserNotFoundException();
    }

    @Transactional
    @Override
    public List<UserDto> getAll() throws EmptyDataException, SQLException {
        List<User> userDaoAll = userDao.getAll();
        if (userDaoAll != null) {
            return userMapper.toUserDtos(userDaoAll);
        }
        throw new EmptyDataException();
    }

    @Transactional
    @Override
    public void save(UserDto userDto, String password) throws UserAlreadyExistsException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidUserException {

        boolean exists = userDao.exists(userMapper.toUser(userDto));
        if (exists) {
            throw new UserAlreadyExistsException();
        } else if (userDtoValidator.isValid(userDto) && addressDtoValidator.isValid(userDto.getAddress()) && passwordValidator.isValid(password)) {
            User user = userMapper.toUser(userDto);
            user.setPassword(hash.hashPassword(password));
            Address address = addressDao.existingEntity(user.getAddress());
            if (address != null) {
                user.setAddress(address);
            }
            userDao.save(user);
        } else throw new InvalidUserException();
    }

    @Transactional
    @Override
    public void update(UserDto user) throws SQLException {

        userDao.update(userMapper.toUser(user));
    }

    @Transactional
    @Override
    public void delete(int id) throws SQLException {

        userDao.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteByEmail(String email) throws SQLException {
        userDao.deleteByEmail(email);
    }


    public UserDto login(String email, String password) throws SQLException, UserNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException {

       /* User userByEmail = userDao.getByEmail(email);
        if (userByEmail != null) {
            String daoPassword = userByEmail.getPassword();
            if (hash.validatePassword(password, daoPassword)) {
                return userMapper.toUserDto(userByEmail);
            }
        }*/
        throw new UserNotFoundException();
    }

}
