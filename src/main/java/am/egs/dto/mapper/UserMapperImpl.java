package am.egs.dto.mapper;

import am.egs.dto.UserDto;
import am.egs.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserMapperImpl implements UserMapper {
    @Override
    public UserDto toUserDto(User user) {
        return UserDto.newBuilder()
                .setId(user.getId())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setTelephone(user.getTelephone())
                .setEducation(user.getEducation())
                .setEmail(user.getEmail())
                .setAdmin(user.isAdmin())
                .setAddress(new AddressMapperImpl().toAddressDto(user.getAddress())).build();
    }

    @Override
    public List<UserDto> toUserDtos(List<User> users) {
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : users) {
            userDtos.add(toUserDto(user));
        }
        return userDtos;
    }

    @Override
    public User toUser(UserDto userDto) {
        return new User(userDto.getFirstName(), userDto.getLastName(), userDto.getEmail(), userDto.getTelephone(), userDto.getEducation(), userDto.isAdmin(), new AddressMapperImpl().toAddress(userDto.getAddress()));
    }
}
