package am.egs.dto.mapper;

import am.egs.dto.AddressDto;
import am.egs.entity.Address;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface AddressMapper {

    AddressDto toAddressDto(Address address);

    List<AddressDto> toAddressDtos(List<Address> addresses);

    Address toAddress(AddressDto addressDto);

}
