package am.egs.dto.mapper;

import am.egs.dto.AddressDto;
import am.egs.entity.Address;
import am.egs.enums.City;

import java.util.ArrayList;
import java.util.List;

public class AddressMapperImpl implements AddressMapper {
    @Override
    public AddressDto toAddressDto(Address address) {
        return AddressDto.newBuilder()
                .setCity(address.getCity().name())
                .setStreet(address.getStreet())
                .setApt(address.getApt()).build();
    }

    @Override
    public List<AddressDto> toAddressDtos(List<Address> addresses) {
        List<AddressDto> addressDtos = new ArrayList<>();
        for (Address address : addresses) {
            addressDtos.add(toAddressDto(address));
        }
        return addressDtos;
    }

    @Override
    public Address toAddress(AddressDto addressDto) {
        return new Address(City.valueOf(addressDto.getCity()), addressDto.getStreet(), addressDto.getApt());
    }
}
