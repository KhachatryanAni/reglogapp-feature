package am.egs.dto;

public class AddressDto {

    private String city;
    private String street;
    private int apt;

    private AddressDto() {
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getApt() {
        return apt;
    }

    public static AddressDto.Builder newBuilder() {
        return new AddressDto().new Builder();
    }

    public class Builder {
        private Builder() {
        }

        public AddressDto.Builder setCity(String city){
            AddressDto.this.city = city;
            return this;
        }

        public AddressDto.Builder setStreet(String street){
            AddressDto.this.street = street;
            return this;
        }

        public AddressDto.Builder setApt(int apt){
            AddressDto.this.apt = apt;
            return this;
        }
        public AddressDto build() {
            return AddressDto.this;
        }
    }

    @Override
    public String toString() {
        return city + ", " + street + ", apt " + apt;
    }
}
