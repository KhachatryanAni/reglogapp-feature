package am.egs.dto;

import javax.validation.constraints.NotNull;

public class UserDto {

    private int id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String email;

    private String telephone;

    private String education;

    private boolean isAdmin;

    private AddressDto addressDto;

    private UserDto() {
    }

    public UserDto(@NotNull String firstName, @NotNull String lastName, @NotNull String email, String telephone, String education, boolean isAdmin, AddressDto addressDto) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.telephone = telephone;
        this.education = education;
        this.isAdmin = isAdmin;
        this.addressDto = addressDto;
    }

    public static UserDto.Builder newBuilder() {
        return new UserDto().new Builder();
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEducation() {
        return education;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public AddressDto getAddress() {
        return addressDto;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id='" + id + '\'' +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", education='" + education + '\'' +
                ", isAdmin=" + isAdmin +
                ", addressDto=" + addressDto +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public UserDto.Builder setId(int id) {
            UserDto.this.id = id;
            return this;
        }

        public UserDto.Builder setFirstName(String firstName) {
            UserDto.this.firstName = firstName;
            return this;
        }

        public UserDto.Builder setLastName(String lastName) {
            UserDto.this.lastName = lastName;
            return this;
        }

        public UserDto.Builder setEmail(String email) {
            UserDto.this.email = email;
            return this;
        }

        public UserDto.Builder setTelephone(String telephone) {
            UserDto.this.telephone = telephone;
            return this;
        }

        public UserDto.Builder setEducation(String education) {
            UserDto.this.education = education;
            return this;
        }

        public UserDto.Builder setAdmin(boolean admin) {
            UserDto.this.isAdmin = admin;
            return this;
        }

        public UserDto.Builder setAddress(AddressDto addressDto) {
            UserDto.this.addressDto = addressDto;
            return this;
        }

        public UserDto build() {
            return UserDto.this;
        }
    }
}
