package am.egs.enums;

public enum City {
    YEREVAN,
    GYUMRI,
    VANADZOR,
    ARTASHAT,
    ARMAVIR,
    ARARAT,
    ABOVYAN,
    KAPAN,
    IJEVAN,
    DILIJAN
}
