package am.egs.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "user", catalog = "users")
@NamedQueries(
        {@NamedQuery(name = "User.SelectUserByEmail", query = "SELECT new am.egs.dto.LoginDto(u.email, u.password) FROM User u WHERE u.email =:email"),
                @NamedQuery(name = "User.SelectUserById", query = "SELECT new am.egs.dto.UserDto(u.firstName, u.lastName, u.email, u.telephone, u.education, u.isAdmin, u.addressDto) FROM User u WHERE u.id =:id"),
                @NamedQuery(name = "User.SelectAllUsers", query = "SELECT new am.egs.dto.UserDto(u.id, u.firstName, u.lastName, u.email, u.telephone, u.education, u.isAdmin, u.addressDto) FROM User u WHERE u.id =:id")
        })

public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "firstName", nullable = false)
    private String firstName;

    @Column(name = "lastName", nullable = false)
    private String lastName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "telephone", nullable = false)
    private String telephone;

    @Column(name = "education", nullable = false)
    private String education;

    @Column(name = "isAdmin", nullable = false)
    private boolean isAdmin;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Address.class)
    @JoinColumn(name = "address_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Address address;

    public User() {
    }

    public User(String firstName, String lastName, String email, String password, String telephone, String education, boolean isAdmin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.telephone = telephone;
        this.education = education;
        this.isAdmin = isAdmin;
    }

    public User(String firstName, String lastName, String email, String telephone, String education, boolean isAdmin, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.telephone = telephone;
        this.education = education;
        this.isAdmin = isAdmin;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getEmail(), user.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }

}
