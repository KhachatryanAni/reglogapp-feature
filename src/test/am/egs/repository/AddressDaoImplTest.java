package am.egs.repository;

import am.egs.entity.Address;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class AddressDaoImplTest {

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    void get() throws SQLException {
        AddressDao addressDao = new AddressDaoImpl(sessionFactory);

        Address address = addressDao.get(30);
        assertEquals("Yerevan", address.getCity());
    }

    @Test
    void getAll() {
    }

    @Test
    void save(Address address) {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void exists() {
    }
}